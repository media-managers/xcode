## XCODE

Non-destructively transcode all flac files in current directory to either V0 mp3 or 320kbps mp3.

- Copy (most) id3 tags
- If the flac files are 96kHz sample rate and above, convert to 44.1kHz otherwise leave sample rate alone.
- Copy images (no embedded images _yet_)

----

### Requirements
- flac
- lame
- sox

Debian: `sudo apt install flac lame sox`

----

### How to use
- I prefer to `ln -s` the script to a directory in `$PATH` so I can run it from anywhere

#### Options
Use `xcode --help` for a list of options. You can transcode in place or send them to a specified directory.

The script also supports recursive transcoding with the `-r` or `--recursive` flag.
- `cd` to the directory with the flac files you want converted
- run the script
- enjoy

### Flags
* `xcode --help` to display help
* `xcode --new-dir` to send files to (specified in script) directory
  * Must set $mp3dir variable in script
  * Defaults to the directory that the flac files are in.
* `xcode --bitdepth=320` for 320kbps mp3
* `xcode --bitdepth=V0` for V0 mp3 (default)
* `xcode --recursive` to recurse through all directories.
  * Haven't tried combining `-r` and `-d` yet.

----

#### Links
- http://id3.org/id3v2.4.0-frames
- https://picard.musicbrainz.org/docs/mappings/#cite_note-0
- https://hydrogenaud.io/index.php?topic=112504.0
